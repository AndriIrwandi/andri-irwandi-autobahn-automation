/// <reference types="Cypress" />

describe('Signup Page', () => {
    beforeEach(() => {
        cy.visit('https://autobahn.security/signup');
    })
  
    it('Should successfully sign up with valid email and password', () => {
        const validEmail = 'ofkjjwhit@bugfoo.com';
        const validPassword = 'Andriirwandi12!';
        const firstName = 'Andri';
        const lastName = 'Irwandi';
        const chooseIndustry = 'Banking & Finance';
        const phoneNumber = '089507609651';

        cy.get('input[name="email"]').type(validEmail);
        cy.get('input[name="password"]').type(validPassword);
        cy.get('.button-wrapper > .custom-button').click();
  
        // Assert that the form transitions to the "Add your info to make collaborating easy" section
        cy.contains('Add your info to make collaborating easy', { timeout: 7000 }).should((section) => {
            expect(section).to.be.visible;
        });

        // Filled in the First Name & Last Name
        cy.get('input[name="first-name"]').type(firstName);
        cy.get('input[name="last-name"]').type(lastName);
    
        // Validate the Industry value
        let countIndustry = 0;
        const IndustryData = [
            'Banking & Finance', 'Commercial Services', 'Consumer Products', 'Technology Services', 'Hardware', 'Insurance',
            'Machinery & Automotive', 'Materials & Chemicals', 'Media & Entertainment', 'Healthcare & Pharma',
            'Real Estate & Construction', 'Software', 'Energy & Utilities', 'Transport & Logistics', 'Food & Hospitality',
            'Online Services', 'Professional Services', 'Consumer Services'
        ];
  
        cy.get('.menu > .items').then(industry => {
            // Loop industry options
            for (let i = 0; i < industry.length; i++) {
                cy.wrap(industry[i]).invoke('text').then(getIndustry => {
                    cy.log(getIndustry);
        
                    // Validate the value of the array
                    if (IndustryData.includes(getIndustry)) {
                        countIndustry++;
                    }
        
                    // Validate the total value of the array
                    if (i === industry.length - 1) {
                        expect(countIndustry).to.equal(IndustryData.length);
                        cy.log('Industry Value Passed');
                    }
                })
            }
        })

        // Click and choose the Industry Option
        cy.get('.toggler')
        .click() 
        .then(() => {
            cy.contains('.items', chooseIndustry).click(); // Click on the desired option
        });

        // Filled in the Phone Number field
        cy.get('.input-set > .iti > .iti__flag-container').click();
        cy.get('#iti-item-id').click();
        cy.get('.iti > .form-control').type(phoneNumber);

        // Hit the "Start Using Autobahn" button
        cy.get('.button-wrapper > .custom-button > .btn > .button-text').click();

    })
})
  